Source: python-pamqp
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders:
 Georg Faerber <georg@debian.org>,
 Michael Fladischer <fladi@debian.org>,
Build-Depends:
 architecture-is-64-bit,
 debhelper-compat (= 13),
 dh-sequence-python3,
 pybuild-plugin-pyproject,
 python3-all,
 python3-doc,
 python3-pytest,
 python3-setuptools,
 python3-sphinx,
 python3-sphinx-rtd-theme,
Standards-Version: 4.7.0
Homepage: https://github.com/gmr/pamqp/
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-pamqp
Vcs-Git: https://salsa.debian.org/python-team/packages/python-pamqp.git
Testsuite: autopkgtest-pkg-python
Rules-Requires-Root: no

Package: python-pamqp-doc
Section: doc
Architecture: all
Depends:
 ${misc:Depends},
 ${sphinxdoc:Depends},
Multi-Arch: foreign
Description: RabbitMQ Focused AMQP low-level library (Documentation)
 pamqp is a low level AMQP 0-9-1 frame encoding and decoding library for Python.
 It is not a end-user client library for talking to RabbitMQ but rather is used
 by client libraries for marshaling and unmarshaling AMQP frames.
 .
 AMQP class/method command class mappings can be found in the
 pamqp.specification module while actual frame encoding and encoding should be
 run through the pamqp.frame module.
 .
 This package contains the documentation.

Package: python3-pamqp
Architecture: any
Depends:
 ${misc:Depends},
 ${python3:Depends},
Suggests:
 python-pamqp-doc,
Description: RabbitMQ Focused AMQP low-level library (Python3 version)
 pamqp is a low level AMQP 0-9-1 frame encoding and decoding library for Python.
 It is not a end-user client library for talking to RabbitMQ but rather is used
 by client libraries for marshaling and unmarshaling AMQP frames.
 .
 AMQP class/method command class mappings can be found in the
 pamqp.specification module while actual frame encoding and encoding should be
 run through the pamqp.frame module.
 .
 This package contains the Python 3 version of the library.
